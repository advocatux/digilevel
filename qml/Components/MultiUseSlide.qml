import QtQuick 2.9
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3

RowLayout {
    property alias caption: innerLabel1.text
    property alias boxColor: _sliderBox.color
    property alias value: _sliderControl.value
    property alias maxVal:  _sliderControl.maximumValue
    property alias stepSize: _sliderControl.stepSize

    Rectangle {
        height: sliderHeight ; width: sliderWidth; radius: localButtonRadius
        color: theme.palette.normal.background
        border.color: theme.palette.normal.backgroundTertiaryText
        border.width: localButtonBorder
        Layout.fillWidth: true

        Label {
            id: innerLabel1
            text: "R"
            color: theme.palette.normal.backgroundText
            anchors.centerIn: parent
        }
        MouseArea {
            anchors.fill: parent
            //onClicked: { resetColors() }
        }
    }
    Rectangle {
        id: _sliderBox

        height: sliderHeight ; width: root.width / 1.3 - units.gu(2) ; radius: localButtonRadius
        color: 'red'
        border.color: theme.palette.normal.backgroundTertiaryText
        border.width: localButtonBorder
        Layout.fillWidth: true

        Slider {
            id:_sliderControl
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            Layout.maximumWidth: root.width /4 ; Layout.preferredHeight: units.gu(3.5)
            maximumValue: 255.0
            stepSize: 1.0
            onValueChanged: { 
                if (maximumValue == 255.0) {
                    setSwatch() ;
                }
            }
        }
    }
    Rectangle {
        height: sliderHeight ; width: sliderWidth * 2; radius: localButtonRadius
        color: theme.palette.normal.background
        border.color: theme.palette.normal.backgroundTertiaryText
        border.width: localButtonBorder
        Layout.fillWidth: true

        Label {
            id: innerLabel3
            text: Math.round(_sliderControl.value)
            color: theme.palette.normal.backgroundText
            anchors.centerIn: parent
        }
    }
}

