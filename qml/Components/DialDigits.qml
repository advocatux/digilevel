import QtQuick 2.9

Repeater {
//    property var romanDigit: ["•","•","•","•","➖","•","•","•","•"," ","•","•","•","•","➖","•","•","•","•","v"]
    property var markers: ["•","•","•","•","➖"," "," "," "," "," "," "," "," "," ","➖","•","•","•","•","v"]
    model: 20
    delegate: Item {
        width: parent.border.width
        height: parent.radius - width * 2
        rotation: 18 * (index + 1)
        transformOrigin: Item.Bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.verticalCenter

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            text: markers[index]
            rotation: -parent.rotation
            font.pixelSize: parent.width * 4
            color: _textColor
        }
    }
}
